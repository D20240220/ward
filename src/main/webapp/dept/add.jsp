
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String baseUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";
%>
<html>
<head>
    <base href="<%=baseUrl%>">
    <title>录入部门信息</title>
    <link rel="stylesheet" href="layui/css/layui.css">
</head>
<body>
<form class="layui-form">
    <div class="layui-form-item">
        <label class="layui-form-label">部门名</label>
        <div class="layui-input-inline">
            <input class="layui-input" type="text" name="name" placeholder="请输入部门名">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">办公室</label>
        <div class="layui-input-inline">
            <input class="layui-input" type="text" name="room" placeholder="请输入部门办公室">
        </div>
    </div>


    <div class="layui-form-item">
        <label class="layui-form-label">部门电话</label>
        <div class="layui-input-inline">
            <input class="layui-input" type="text" name="phone" placeholder="请输入部门的电话">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">邮箱</label>
        <div class="layui-input-inline">
            <input class="layui-input" type="email" name="email" placeholder="请输入部门的邮箱">
        </div>
    </div>


    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="add">录入</button>
        </div>
    </div>
</form>

<script src="layui/layui.js"></script>
<script>

    layui.use(['form'], function () {
        var form = layui.form;
        var $ = layui.$;



        //表单的提交
        form.on('submit(add)', function (data) {
            $.ajax({
                url: 'dept?action=add',
                data: data.field,
                type: 'post',
                dateType: 'json',
                success(result) {
                    if (result.code == 0) {
                        layer.msg('添加成功', {icon: 6}, function (index) {
                            parent.layer.closeAll('iframe');

                        })
                    }else {
                        layer.msg(result.msg, {icon: 5});
                    }
                },
                error() {
                    layer.msg('请求失败', {icon: 5});
                }
            });

            //阻止表单自动提交
            return false;
        });
    });
</script>

</body>
</html>
