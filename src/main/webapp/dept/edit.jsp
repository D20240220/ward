
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String baseUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";
%>
<html>
<head>
    <base href="<%=baseUrl%>">
    <title>编辑部门信息</title>
    <link rel="stylesheet" href="layui/css/layui.css">
</head>
<body>
<form class="layui-form">
    <div class="layui-form-item">
        <label class="layui-form-label">科室</label>
        <div class="layui-input-inline">
            <input class="layui-input" type="text" name="name" placeholder="请输入员工的姓名">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">办公地址</label>
        <div class="layui-input-inline">
            <input class="layui-input" type="text" name="room" placeholder="请输入员工的姓名">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">电话</label>
        <div class="layui-input-inline">
            <input class="layui-input" type="text" name="phone" placeholder="请输入员工的电话">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">邮箱</label>
        <div class="layui-input-inline">
            <input class="layui-input" type="email" name="email" placeholder="请输入员工的邮箱">
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <input type="hidden" name="id" value="${param.id}">
            <button class="layui-btn" lay-submit lay-filter="edit">编辑</button>
        </div>
    </div>
</form>

<script src="layui/layui.js"></script>
<script>

    layui.use(['form'], function () {
        var form = layui.form;
        var $ = layui.$;

        //数据回显  根据ID得到信息
        getDept(${param.id});//EL表达式   获取id的参数

        //获取指定id的部门信息
        function getDept(id){
            $.ajax({
                url: 'dept?action=getById',
                data: {id: id},
                type: 'get',
                dataType: 'json',
                success(result) {
                    if (result.code==0){
                        //数据回显
                        $('input[name="name"] ').val(result.data.name);
                        $('input[name="room"] ').val(result.data.room);
                        $('input[name="phone"] ').val(result.data.phone);
                        $('input[name="email"] ').val(result.data.email);
                        //$('input[name="status"][value="'+result.data.status+'"]').attr('checked','checked');
                    }else {
                        layer.msg(result.msg,{icon:5});
                    }
                },
                error(){
                    layer.msg('请求失败',{icon:5});
                }
            });
        }


        //表单的提交
        form.on('submit(edit)', function (data) {
            $.ajax({
                url: 'dept?action=edit',
                data: data.field,
                type: 'post',
                dateType: 'json',
                success(result) {
                    if (result.code == 0) {
                        layer.msg('编辑成功', {icon: 6}, function (index) {
                            parent.layer.closeAll('iframe');
                        })
                    }else {
                        layer.msg(result.msg, {icon: 5});
                    }
                },
                error() {
                    layer.msg('请求失败', {icon: 5});
                }
            });

            //阻止表单自动提交
            return false;
        });
    });
</script>

</body>
</html>
