
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String baseUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";
%>
<html>
<head>
    <base href="<%=baseUrl%>">
    <title>编辑员工信息</title>
    <link rel="stylesheet" href="layui/css/layui.css">
</head>
<body>
<form class="layui-form">
    <div class="layui-form-item">
        <label class="layui-form-label">姓名</label>
        <div class="layui-input-inline">
            <input class="layui-input" type="text" name="name" placeholder="请输入员工的姓名">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">性别</label>
        <div class="layui-input-inline">
            <input type="radio" name="sex" value="男" title="男" >
            <input type="radio" name="sex" value="女" title="女">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">电话</label>
        <div class="layui-input-inline">
            <input class="layui-input" type="text" name="phone" placeholder="请输入员工的电话">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">邮箱</label>
        <div class="layui-input-inline">
            <input class="layui-input" type="email" name="email" placeholder="请输入员工的邮箱">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">角色</label>
        <div class="layui-input-inline">
            <input type="radio" name="role" value="0" title="员工" >
            <input type="radio" name="role" value="1" title="管理员">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">部门</label>
        <div class="layui-input-inline">
            <select name="dept_id"></select>
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <input type="hidden" name="id" value="${param.id}">
            <button class="layui-btn" lay-submit lay-filter="edit">编辑</button>
        </div>
    </div>
</form>

<script src="layui/layui.js"></script>
<script>

    layui.use(['form'], function () {
        var form = layui.form;
        var $ = layui.$;

        //数据回显  根据ID得到信息
        getStaff(${param.id});//EL表达式   获取id的参数

        //获取指定id的员工信息
        function getStaff(id){
            $.ajax({
                url: 'staff?action=getById',
                data: {id: id},
                type: 'get',
                dataType: 'json',
                success(result) {
                     if (result.code==0){
                         //数据回显
                         $('input[name="name"] ').val(result.data.name);
                         $('input[name="phone"] ').val(result.data.phone);
                         $('input[name="email"] ').val(result.data.email);
                         $('input[name="sex"][value="'+result.data.sex+'"]').attr('checked','checked');
                         $('input[name="role"][value="'+result.data.role+'"]').attr('checked','checked');
                         getDepts(result.data.dept_id);
                     }else {
                         layer.msg(result.msg,{icon:5});
                     }
                },
                error(){
                    layer.msg('请求失败',{icon:5});
                }
                });
        }

        //获取部门列表
        function getDepts(deptId) {
            $.ajax({
                url: 'dept?action=search',
                type: 'get',
                dateType: 'json',
                success(result) {
                    var html = '<option value="">请选择部门</option>';
                    $.each(result.data, function (index, dept) {
                        if (deptId==dept.id){
                            html += '<option value="' + dept.id + '" selected>' + dept.name + '</option>';
                        }else {
                            html += '<option value="' + dept.id + '">' + dept.name + '</option>';
                        }

                    });
                    $('select[name="dept_id"]').html(html);

                    //渲染
                    form.render();
                },
                error() {
                    layer.msg('请求失败', {icon: 5});
                }
            });
        }

        //表单的提交
        form.on('submit(edit)', function (data) {
            $.ajax({
                url: 'staff?action=edit',
                data: data.field,
                type: 'post',
                dateType: 'json',
                success(result) {
                    if (result.code == 0) {
                        layer.msg('编辑成功', {icon: 6}, function (index) {
                            parent.layer.closeAll('iframe');

                        })
                    }else {
                        layer.msg(result.msg, {icon: 5});
                    }
                },
                error() {
                    layer.msg('请求失败', {icon: 5});
                }
            });

            //阻止表单自动提交
            return false;
        });
    });
</script>

</body>
</html>
