<%--
  Created by IntelliJ IDEA.
  User: Hasee
  Date: 2024/1/23
  Time: 19:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String baseUrl = request.getScheme() + "://" + request.getServerName() + ":"
            + request.getServerPort() + request.getContextPath() + "/";
%>
<html>
<head>
    <base href="<%=baseUrl%>">
    <title>编辑个人信息</title>
    <link rel="stylesheet" href="layui/css/layui.css">
</head>
<body>
<form class="layui-form">
    <div class="layui-form-item">
        <label class="layui-form-label">电话</label>
        <div class="layui-input-inline">
            <input class="layui-input" type="text" name="phone"
                   placeholder="请输入电话" value="${sessionScope.staff.phone}">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">邮箱</label>
        <div class="layui-input-inline">
            <input class="Layui-input" type="email" name="email"
                   placeholder="请输入邮箱" value="${sessionScope.staff.email}">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">头像</label>
        <div class="layui-input-inline">
            <input type="hidden" name="head" value="${sessionScope.staff.head}">
            <button type="button" class="layui-btn" id="ID-upload-demo-btn">
                <i class="layui-icon layui-icon-upload"></i> 单图片上传
            </button>
            <div style="width: 132px;">
                <div class="layui-upload-list">
                    <img src="upload?head=${sessionScope.staff.head}"
                            class="layui-upload-img" id="ID-upload-demo-img" style="width: 100%; height: 92px;">
                    <div id="ID-upload-demo-text"></div>
                </div>
                <div class="layui-progress layui-progress-big" lay-showPercent="yes" lay-filter="filter-demo">
                    <div class="layui-progress-bar" lay-percent=""></div>
                </div>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="modify">提交</button>
        </div>
    </div>
</form>

<script src="layui/layui.js"></script>
<script>
    //写JS文件

    layui.use(['form','upload','element'],function(){
        var upload = layui.upload;
        var element = layui.element;
        var form=layui.form;
        var $ = layui.$;
        // 单图片上传
        var uploadInst = upload.render({
            elem: '#ID-upload-demo-btn',
            url: 'upload',               // 实际使用时改成您自己的上传接口即可。
            before: function (obj) {
                // 预读本地文件示例，不支持ie8
                obj.preview(function (index, file, result) {
                    $('#ID-upload-demo-img').attr('src', result); // 图片链接（base64）
                });

                element.progress('filter-demo', '0%'); // 进度条复位
                layer.msg('上传中', {icon: 16, time: 0});
            },
            done: function (res) {
                // 若上传失败
                if (res.code > 0) {
                    return layer.msg('上传失败');
                }
                // 上传成功的一些操作
                $('[name="head"]').val(res.msg);                // …
                $('#ID-upload-demo-text').html(''); // 置空上传失败的状态
            },
            error: function () {
                // 演示失败状态，并实现重传
                var demoText = $('#ID-upload-demo-text');
                demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-xs demo-reload">重试</a>');
                demoText.find('.demo-reload').on('click', function () {
                    uploadInst.upload();
                });
            },
            // 进度条
            progress: function (n, elem, e) {
                element.progress('filter-demo', n + '%'); // 可配合 layui 进度条元素使用
                if (n == 100) {
                    layer.msg('上传完毕', {icon: 1});
                }
            }
        });

        //表单提交
        form.on('submit(modify)',function (data){
            $.ajax({
                url: 'staff?action=modify',//请求地址
                data: data.field,//提交的参数
                type: 'post',//请求的类型
                dataType: 'json',//返回数据的类型
                success(result){//请求成功时的回调，result返回的结果
                    if (result.code == 0){
                        layer.msg('修改成功',{icon:6},function (){
                            //刷新页面
                            parent.window.location.href="staff/index.jsp";
                        });
                    }else {
                        layer.msg(result.msg,{icon:5});
                    }
                },
                error(){
                    layer.msg('请求失败',{icon:5})
                }
            });
            return false;
        });
    });


</script>


</body>
</html>
