<%--
  Created by IntelliJ IDEA.
  User: Hasee
  Date: 2024/1/23
  Time: 9:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String baseUrl = request.getScheme() + "://" + request.getServerName() + ":"
            + request.getServerPort() + request.getContextPath() + "/";
%>
<%--判断用户是否登录
    登录则啥都不干
    未登录则跳转到登陆界面
    从session中--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
    <base href="<%=baseUrl%>">
    <meta charset="utf-8">
    <title>病房管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="layui/css/layui.css" rel="stylesheet">
</head>
<body>
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo layui-hide-xs layui-bg-black">病房管理系统</div>
        <!-- 头部区域（可配合layui 已有的水平导航） -->
        <ul class="layui-nav layui-layout-left">
            <!-- 移动端显示 -->
            <li class="layui-nav-item layui-show-xs-inline-block layui-hide-sm" lay-header-event="menuLeft">
                <i class="layui-icon layui-icon-spread-left"></i>
            </li>
<%--            <li class="layui-nav-item layui-hide-xs"><a href="javascript:;">nav 1</a></li>--%>
<%--            <li class="layui-nav-item layui-hide-xs"><a href="javascript:;">nav 2</a></li>--%>
<%--            <li class="layui-nav-item layui-hide-xs"><a href="javascript:;">nav 3</a></li>--%>
<%--            <li class="layui-nav-item">--%>
<%--                <a href="javascript:;">nav groups</a>--%>
<%--                <dl class="layui-nav-child">--%>
<%--                    <dd><a href="javascript:;">menu 11</a></dd>--%>
<%--                    <dd><a href="javascript:;">menu 22</a></dd>--%>
<%--                    <dd><a href="javascript:;">menu 33</a></dd>--%>
<%--                </dl>--%>
<%--            </li>--%>
        </ul>
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item layui-hide layui-show-sm-inline-block">
                <a href="javascript:;">
                    <img src="upload?head=${sessionScope.staff.head}" class="layui-nav-img">
                    ${sessionScope.staff.name}
                    <c:if test="${sessionScope.staff.role == 0}">用户</c:if >
                    <c:if test="${sessionScope.staff.role == 1}">管理员</c:if >
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="javascript:;" class="edit"><i class="layui-icon layui-icon-edit" ></i>修改信息</a></dd>
                    <dd><a href="javascript:;" class="resetPwd"><i class="layui-icon layui-icon-key" ></i>修改密码</a></dd>
                    <dd><a href="staff?action=logout" class="edit"><i class="layui-icon layui-icon-logout" ></i>退出登录</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item" lay-header-event="menuRight" lay-unselect>
                <a href="javascript:;">
                    <i class="layui-icon layui-icon-more-vertical"></i>
                </a>
            </li>
        </ul>
    </div>
    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree" lay-filter="test">
                <li class="layui-nav-item layui-nav-itemed">
                    <a class="" href="javascript:;">部门管理</a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:"  data-url="dept/list.jsp">管理部门</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">员工管理</a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:"  data-url="staff/list.jsp">管理员工</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">病房管理</a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:;" data-url="ward/list.jsp">管理病房</a></dd>
                    </dl>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:;" data-url="bed/list.jsp">管理病床</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">患者管理</a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:;" data-url="history/list.jsp">管理患者</a></dd>
                    </dl>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:;" data-url="history/oldlist.jsp">管理病例</a></dd>
                    </dl>
                </li>

            </ul>
        </div>
    </div>
    <div class="layui-body">
        <div style="padding: 15px;width: 100%;height: 100%;border-width:0px" >
            <iframe id="frame" style="width: 100%;height: 100%;border-width:0px; "></iframe>

        </div>
    </div>


</div>

<script src="layui/layui.js"></script>
<script>
    //JS
    layui.use(['element', 'layer', 'util','form'], function(){
        var element = layui.element;
        var layer = layui.layer;
        var util = layui.util;
        var form = layui.form;  //引用layui的form组件
        var $ = layui.$;//就是jQuery
        //修改信息
        $('.edit').click(function (){
            layer.open({
                title:'修改信息',
                area:['800px','500px'],
                type:2,//iframe  子窗口
                content:'staff/modify.jsp',
            });
        });
        //修改密码
        $('.resetPwd').click(function (){
            // 在此处输入 layer 的任意代码
            layer.open({
                type: 1, // page 层类型
                area: ['500px', '300px'],//大戏
                title: '修改密码',//标题
                content: '<div style="padding: 15px;"> \
                                <form action="staff?action=resetPwd" method="post">\
                                    <div class="layui-form-item">\
                                        <label class="layui-form-label">旧密码</label>\
                                        <div class="layui-input-inline">\
                                            <input class="layui-input" type="password" name="oldPwd" placeholder="请输入旧密码">\
                                        </div>\
                                    </div>\
                                    <div class="layui-form-item">\
                                        <label class="layui-form-label">新密码</label>\
                                        <div class="layui-input-inline">\
                                            <input class="layui-input" type="password" name="newPwd" placeholder="请输入新密码">\
                                        </div>\
                                    </div>\
                                    <div class="layui-form-item">\
                                        <label class="layui-form-label">确认密码</label>\
                                        <div class="layui-input-inline">\
                                            <input class="layui-input" type="password" name="rePwd" placeholder="请确认密码">\
                                        </div>\
                                    </div>\
                                    <div class="layui-form-item">\
                                        <div class="layui-input-block">\
                                            <button class="layui-btn" lay-submit lay-filter="resetPwd">修改</button>\
                                        </div>\
                                    </div>\
                                </form>\
                           </div>'

            });
        });

        //表单的提交操作
        form.on('submit(resetPwd)',function (data){
            //手动获取表单数据
            var oldPwd = $('[name="oldPwd"]').val();
            var newPwd = $('[name="newPwd"]').val();
            var rePwd = $('[name="rePwd"]').val();


            //ajax发送修改密码的请求
            $.ajax({
                url:'staff?action=resetPwd',//请求路径
                data:{oldPwd,newPwd,rePwd},//要传递参数
                type:'post',
                dataType:'json',
                success:function (result){
                    if (result.code == 0){
                        //成功
                        layer.msg('修改成功',{icon:6},function (index){
                            //跳转到登陆页面
                            window.location.href="staff/login.jsp"
                        });
                    }else {
                        //失败
                        layer.msg(result.msg,{icon:5})                    }

                },
                error:function (){
                    layer.msg('请求失败',{icon:5});
                }
            });
            //阻止表单自动提交
            return false;
        });

        //左侧菜单事件
        $('.layui-nav-tree dd a').click(function (){
            var url = $(this).attr('data-url');
            if(url){
                //打开
                $('#frame').attr('src',url);
            }else {
                layer.msg('无效的URL');
            }
        });

        //头部事件
        util.event('lay-header-event', {
            menuLeft: function(othis){ // 左侧菜单事件
                layer.msg('展开左侧菜单的操作', {icon: 0});
            },
            menuRight: function(){  // 右侧菜单事件
                layer.open({
                    type: 1,
                    title: '更多',
                    content: '<div style="padding: 15px;">处理右侧面板的操作</div>',
                    area: ['260px', '100%'],
                    offset: 'rt', // 右上角
                    anim: 'slideLeft', // 从右侧抽屉滑出
                    shadeClose: true,
                    scrollbar: false
                });
            }
        });
    });
</script>
</body>
</html>
