<%--
  Created by IntelliJ IDEA.
  User: Hasee
  Date: 2024/1/22
  Time: 16:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isErrorPage="false" %>
<%
    String baseUrl = request.getScheme() + "://" + request.getServerName()+":"
            +request.getServerPort()+request.getContextPath()+"/";
%>
<html>
<head>
    <base href="<%=baseUrl%>">
    <title>Title</title>
    <link rel="stylesheet" href="layui/css/layui.css">
    <style>
        body{
            background-image: url("img/bg.jpg");
            background-repeat:no-repeat ;
            background-position:center ;
            background-size:cover ;
            background-attachment:fixed ;
        }
        form{
            width: 450px;
            margin: 10% auto;
            background-color: #fff5;;
            border: 1px solid #333;
            border-radius: 15px;
            padding-bottom: 15px;
            box-shadow: 0px 0px 30px;
        }
        h1{
            text-align: center;
            padding: 20px;
        }
        .error{
            text-align: center;
            color:#ff5722;
        }
        .verCode{
            cursor: pointer;
        }
    </style>
</head>
<body>
    <form class="layui-form" action="staff?action=login" method="post">
        <h1>员工登录</h1>
        <div class="error">${error}</div>
        <div class="layui-form-item">
            <label class="layui-form-label">工号</label>
            <div class="layui-input-inline">
                <input class="layui-input" type="text" name="id" placeholder="请输入工号" required>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">密码</label>
            <div class="layui-input-inline">
                <input class="layui-input" type="password" name="password" placeholder="请输入密码" required>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">验证码</label>
            <div class="layui-input-inline" style="width: 80px;">
                <input class="layui-input" type="text" name="verCode" placeholder="验证码" required>
            </div>
            <img class="verCode" src="verCode" title="点击刷新">
        </div>
        <div class="layui-form-item">
            <input type="checkbox" name="autoLogin" lay-skin="primary" title="记住我">
            <a href="#forget" style="float: right; margin-top: 7px;">忘记密码？</a>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn">登录</button>
            </div>
        </div>
    </form>
    <script src="layui/layui.js"></script>
    <script>
        //防止套娃
        (function(window){
            if (window.location!=window.top.location){
                window.top.location=window.location;
            }
        })(this);


        document.getElementsByClassName("verCode")[0].onclick=function(){
            this.setAttribute("src","verCode?"+new Date().getTime());
        }
    </script>
</body>
</html>
