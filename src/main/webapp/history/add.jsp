
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String baseUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";
%>
<html>
<head>
    <base href="<%=baseUrl%>">
    <title>录入患者信息</title>
    <link rel="stylesheet" href="layui/css/layui.css">
</head>
<body>
<form class="layui-form">
    <div class="layui-form-item">
        <label class="layui-form-label">姓名</label>
        <div class="layui-input-inline">
            <input class="layui-input" type="text" name="name" placeholder="请输入姓名">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">身份证号</label>
        <div class="layui-input-inline">
            <input class="layui-input" type="text" name="id_card" placeholder="请输入身份证号">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">科室</label>
        <div class="layui-input-inline">
            <select name="dept_id" lay-filter="dept_id"></select>
        </div>
    </div>
    <div class="yi" style="display: none">
        <div class="layui-form-item">
            <label class="layui-form-label">房间号</label>
            <div class="layui-input-inline">
                <select name="ward" lay-filter="ward"></select>
            </div>
        </div>
        <div class="er" style="display: none">
            <div class="layui-form-item">
                <label class="layui-form-label">床位号</label>
                <div class="layui-input-inline">
                    <select name="bed"></select>
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">主治医生</label>
            <div class="layui-input-inline">
                <select name="staffname"></select>
            </div>
        </div>
    </div>

    <div class="layui-inline">
        <label class="layui-form-label">日期时间</label>
        <div class="layui-input-inline">
            <input type="text" class="layui-input" name="in" id="ID-laydate-shortcut-datetime">
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="add">录入</button>
        </div>
    </div>
</form>

<script src="layui/layui.js"></script>
<script>

    layui.use(['form'], function () {
        var form = layui.form;
        var $ = layui.$;
        var laydate = layui.laydate;
        var util = layui.util;

        laydate.render({
            elem: "#ID-laydate-shortcut-datetime",
            type: "datetime",
            shortcuts: [
                {
                    text: "昨天",
                    value: function(){
                        var now = new Date();
                        now.setDate(now.getDate() - 1);
                        return now;
                    }
                },
                {
                    text: "今天",
                    value: function(){
                        return Date.now();
                    }
                },
                {
                    text: "明天",
                    value: function(){
                        var now = new Date();
                        now.setDate(now.getDate() + 1);
                        return now;
                    }
                },
                {
                    text: "上个月",
                    value: function(){
                        var now = new Date();
                        var month = now.getMonth() - 1;
                        now.setMonth(month);
                        // 若上个月数不匹配，则表示天数溢出
                        if (now.getMonth() !== month) {
                            now.setDate(0); // 重置天数
                        }
                        return [now];
                    }
                },
                {
                    text: "下个月",
                    value: function(){
                        var now = new Date();
                        var month = now.getMonth() + 1;
                        now.setMonth(month);
                        // 若上个月数不匹配，则表示天数溢出
                        if (now.getMonth() !== month) {
                            now.setDate(0); // 重置天数
                        }
                        return [now];
                    }
                },
                {
                    text: "某一天",
                    value: "2016-10-14 10:00:00"
                }
            ]
        });


        //调用函数
        getDepts();
        //获取部门列表
        function getDepts() {
            $.ajax({
                url: 'dept?action=search',
                type: 'get',
                dateType: 'json',
                success(result) {
                    var html = '<option value="">请选择部门</option>';
                    $.each(result.data, function (index, dept) {
                        html += '<option value="' + dept.id + '">' + dept.name + '</option>';
                    });
                    //坊间

                    //主治医生
                    //getStaff()
                    $('select[name="dept_id"]').html(html);

                    //渲染
                    form.render();
                },
                error() {
                    layer.msg('请求失败', {icon: 5});
                }
            });
        }

        function getStaff(dept_id) {
            $.ajax({
                url: 'staff?action=search1',
                data: {deptId:dept_id},
                type: 'get',
                dateType: 'json',
                success(result) {
                    var html = '<option value="">请选择主治医师</option>';
                    $.each(result.data, function (index, staff) {
                        html += '<option value="' + staff.id + '">' + staff.name + '</option>';
                    });
                    // 床位
                    //getBed();
                    $('select[name="staffname"]').html(html);
                    //渲染
                    form.render();
                },
                error() {
                    layer.msg('请求失败', {icon: 5});
                }
            });
        }
        function getWard(dept_id) {
            $.ajax({
                url: 'ward?action=search1',
                data: {deptId:dept_id},
                type: 'get',
                dateType: 'json',
                success(result) {
                    var html = '<option value="">请选择房间</option>';
                    $.each(result.data, function (index, ward) {
                        html += '<option value="' + ward.id + '">' + ward.name + '</option>';
                    });
                    // 床位
                    //getBed();
                    $('select[name="ward"]').html(html);

                    //渲染
                    form.render();
                },
                error() {
                    layer.msg('请求失败', {icon: 5});
                }
            });
        }
        function getBed(ward_id) {
            $.ajax({
                url: 'bed?action=search1',
                data: {wardId:ward_id},
                type: 'get',
                dateType: 'json',
                success(result) {
                    var html = '<option value="">请选择床号</option>';
                    $.each(result.data, function (index, bed) {
                        html += '<option value="' + bed.id + '">' + bed.bed + '</option>';
                    });
                    // 床位
                    //getBed();
                    $('select[name="bed"]').html(html);

                    //渲染
                    form.render();
                },
                error() {
                    layer.msg('请求失败', {icon: 5});
                }
            });
        }

        //表单的提交
        form.on('submit(add)', function (data) {
            $.ajax({
                url: 'history?action=add',
                data: data.field,
                type: 'post',
                dateType: 'json',
                success(result) {
                    if (result.code == 0) {
                        layer.msg('添加成功', {icon: 6}, function (index) {
                            parent.layer.closeAll('iframe');
                        })
                    }else {
                        layer.msg(result.msg, {icon: 5});
                    }
                },
                error() {
                    layer.msg('请求失败', {icon: 5});
                }
            });

            //阻止表单自动提交
            return false;
        });

        form.on('select(dept_id)',(data)=>{
            if (data.value) {
                $('.yi').css("display", 'block');
                getWard(data.value);
                getStaff(data.value);
            }else {
                $('.yi').css("display", 'none');
            }
        });

        form.on('select(ward)',(data)=>{
            if (data.value) {
                $('.er').css('display', 'block');
                getBed(data.value);
            } else {
                $('.er').css('display', 'none');
            }
        });
    });
</script>

</body>
</html>
