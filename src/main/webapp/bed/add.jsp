<%--
  Created by IntelliJ IDEA.
  User: Hasee
  Date: 2024/1/24
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String baseUrl = request.getScheme() + "://" + request.getServerName() + ":"
            + request.getServerPort() + request.getContextPath() + "/";
%>

<html>
<head>
    <base href="<%=baseUrl%>">
    <title>员工列表</title>
    <link rel="stylesheet" href="layui/css/layui.css">
    <style>
        .layui-table-cell{
            height: auto;
        }
    </style>
</head>
<body>
<blockquote class="layui-elem-quote">
    员工列表
</blockquote>
<form class="layui-form layui-form-pane">
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">姓名</label>
            <div class="layui-input-inline">
                <input class="layui-input" name="name" placeholder="请输出姓名">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">性别</label>
            <div class="layui-input-inline">
                <select name="sex">
                    <option value="">不限</option>
                    <option value="男">男</option>
                    <option value="女">女</option>
                </select>
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">角色</label>
            <div class="layui-input-inline">
                <select name="role">
                    <option value="">不限</option>
                    <option value="0">员工</option>
                    <option value="1">管理员</option>
                </select>
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">状态</label>
            <div class="layui-input-inline">
                <select name="status">
                    <option value="">不限</option>
                    <option value="0">在职</option>
                    <option value="1">离职</option>
                </select>
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">部门</label>
            <div class="layui-input-inline">
                <select name="dept_id">
                </select>
            </div>
        </div>
        <div class="layui-inline">
            <div class="layui-input-inline">
                <button class="layui-btn" lay-submit lay-filter="search">
                    <i class="layui-icon layui-icon-search"></i>
                </button>
            </div>
        </div>

    </div>
</form>

<table id="staff-list" lay-filter="staff-list"></table>
<%--表格工具条--%>
<script type="text/html" id="toolbar">
    <div class="layui-btn-container">
        <button class="layui-btn layui-btn-sm" lay-event="add">录入员工</button>
        <button class="layui-btn layui-btn-sm" lay-event="getCheckData">获取选中行数据</button>
        <button class="layui-btn layui-btn-sm" lay-event="getData">获取当前页数据</button>
        <button class="layui-btn layui-btn-sm" id="dropdownButton">
            下拉按钮
            <i class="layui-icon layui-icon-down layui-font-12"></i>
        </button>
        <button class="layui-btn layui-btn-sm layui-bg-blue" id="reloadTest">
            重载测试
            <i class="layui-icon layui-icon-down layui-font-12"></i>
        </button>
        <button class="layui-btn layui-btn-sm layui-btn-primary" id="rowMode">
            <span>{{= d.lineStyle ? '多行' : '单行' }}模式</span>
            <i class="layui-icon layui-icon-down layui-font-12"></i>
        </button>
    </div>
</script>
<%--行工具条--%>
<script type="text/html" id="bar">
    <a class="layui-btn layui-btn-sm" lay-event="edit">编辑</a>
    <a class="layui-btn layui-btn-sm layui-bg-red" lay-event="remove">删除</a>
</script>

<script src="layui/layui.js"></script>
<script>
    layui.use(['table','form'],function (){
        var table=layui.table;
        var form = layui.form;
        var $=layui.$;

        //调用函数
        getDepts();

        //渲染表格  得有数据

        table.render({
            elem:'#staff-list',
            url:'staff?action=search',
            page:true,
            toolbar:'#toolbar',
            cols:[ [
                {title:'ID',field:'id'},
                {title:'姓名',field:'name'},
                {title:'性别',field:'sex'},
                {title:'电话',field:'phone'},
                {title:'邮箱',field:'email'},
                {title:'照片',field:'head',templet(d){
                        if (d.head){
                            return '<img src = "upload?head='+d.head+'">';
                        }else {
                            return '';
                        }
                    }},
                {title:'角色',field:'role', templet(d){
                        return ['员工','管理员'][d.role];
                    }},
                {title:'状态',field:'status', templet(d){
                        return ['在职','离职'][d.status];
                    }},
                {title:'部门',field:'deptName'},
                {title:'操作',width:160 ,toolbar:'#bar'}
            ] ],
            parseData(result){
                return{
                    code:result.code,
                    msg:result.msg,
                    count:result.data.count,
                    data:result.data.list
                };
            }
        });

        //获取部门列表
        function getDepts() {
            $.ajax({
                url: 'dept?action=search',
                type: 'get',
                dateType: 'json',
                success(result) {
                    var html = '<option value="">请选择部门</option>';
                    $.each(result.data, function (index, dept) {
                        html += '<option value="' + dept.id + '">' + dept.name + '</option>';
                    });
                    $('select[name="dept_id"]').html(html);

                    //渲染
                    form.render();
                },
                error() {
                    layer.msg('请求失败', {icon: 5});
                }
            });
        }
        //添加员工的动作
        function addStaff(){
            //弹窗口
            layer.open({
                title: '录入员工信息',
                area: [ '80%','80%'],
                type: 2,
                content: 'staff/add.jsp',
                end(){
                    table.reloadData( 'staff-list' );
                }
            });
        }
        //修改员工
        function editStaff(staff) {
            layer.open({
                title: '编辑员工信息',
                area: ['80%', '80%'],
                type: 2,
                content: 'staff/edit.jsp?id=' + staff.id,
                end() {
                    table.reloadData('staff-list');
                }
            });
        }


        //表格工具条事件
        table.on('toolbar(staff-list)',function(obj){
            if (obj.event=="add"){
                addStaff();
            }
        });

        //添加表格删除和编辑的点击事件
        table.on('tool(staff-list)',function(row){
            if (row.event=="edit"){
                editStaff(row.data);
            }else if (row.event=="remove"){
                layer.confirm('确认要删除这一个员工吗',function(index){
                    //真的要删除了吗   ajax
                    $.ajax({
                        url:'staff?action=remove',
                        data:{id:row.data.id},
                        type:'post',
                        dataType:'json' ,
                        success(result){
                            if (result.code==0){
                                layer.msg('删除成功',{icon:6},function (){
                                    //刷新表格显示的数据
                                    table.reloadData('staff-list')
                                })
                            }
                        },
                        error(){
                            layer.mag('请求失败',{inon:5});
                        }
                    });
                });

            }
        });

        //搜索事件
        form.on('submit(search)', function (data){
            //刷新表格数据
            table.reloadData('staff-list', {
                where: data.field,
                page: {curr: 1},
            });
            //阻止表单自动提交
            return false;
        });

    });
</script>
</body>
</html>
