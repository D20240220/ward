<%--
  Created by IntelliJ IDEA.
  User: Hasee
  Date: 2024/1/24
  Time: 13:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String baseUrl = request.getScheme() + "://" + request.getServerName() + ":"
            + request.getServerPort() + request.getContextPath() + "/";
%>
<html>
<head>
    <base href="<%=baseUrl%>">
    <title>病床列表</title>
    <link rel="stylesheet" href="layui/css/layui.css">
    <style>
        .layui-table-cell{
            height: auto;
        }
    </style>
</head>
<body>
<blockquote class="layui-elem-quote">
    病床列表
</blockquote>
<table id="bed-list" lay-filter="bed-list"></table>
<%--表格工具条--%>
<script type="text/html" id="toolbar">
    <div class="layui-btn-container">
        <button class="layui-btn layui-btn-sm" lay-event="add">添加床位</button>
    </div>
</script>
<%--行工具条--%>
<script type="text/html" id="bar">
    <a class="layui-btn layui-btn-sm" lay-event="edit">编辑</a>
    <a class="layui-btn layui-btn-sm layui-bg-red" lay-event="remove">删除</a>
</script>

<script src="layui/layui.js"></script>
<script>
    layui.use(['table','form'],function (){
        var table=layui.table;
        var form = layui.form;
        var $=layui.$;
        //渲染表格  得有数据
        table.render({
            elem:'#bed-list',
            url:'bed?action=search',
            page:true,
            toolbar:'#toolbar',
            cols:[ [
                {title:'ID',field:'id'},
                {title:'房间号',field:'name'},
                {title:'床号',field:'bed'},
                {title:'状态',field:'status', templet(d){
                        return ['空闲','使用中'][d.status];
                    }},
                {title:'操作',width:160 ,toolbar:'#bar'},
            ] ],
            parseData(result){
                return{
                    code:result.code,
                    msg:result.msg,
                    count:result.data.count,
                    data:result.data
                };
            }
        });
        table.on('toolbar(bed-list)',function(obj){
            if (obj.event=="add"){
                addDept();
            }
        });

        //添加员工的动作
        function addDept(){
            //弹窗口
            layer.open({
                title: '录入部门信息',
                area: [ '80%','80%'],
                type: 2,
                content: 'dept/add.jsp',
                end(){
                    table.reloadData( 'dept-list' );
                }
            });
        }
        //添加表格删除和编辑的点击事件
        table.on('tool(dept-list)',function(row){
            if (row.event=="edit"){
                editDept(row.data);
            }else if (row.event=="remove"){
                layer.confirm('确认要删除部门吗',function(index){
                    //真的要删除了吗   ajax
                    $.ajax({
                        url:'dept?action=remove',
                        data:{id:row.data.id},
                        type:'post',
                        dataType:'json' ,
                        success(result){
                            if (result.code==0){
                                layer.msg('删除成功',{icon:6},function (){
                                    //刷新表格显示的数据
                                    table.reloadData('dept-list')
                                })
                            }
                        },
                        error(){
                            layer.mag('请求失败',{inon:5});
                        }
                    });
                });

            }
        });
        //修改部门
        function editDept(dept) {
            layer.open({
                title: '编辑部门信息',
                area: ['80%', '80%'],
                type: 2,
                content: 'dept/edit.jsp?id=' + dept.id,
                end() {
                    table.reloadData('dept-list');
                }
            });
        }
    });
</script>
</body>
</html>
