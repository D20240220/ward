package com.situ.ward.dao;

import com.situ.ward.entity.Dept;
import com.situ.ward.entity.History;
import com.situ.ward.entity.Ward;

import java.util.List;
import java.util.Map;

public interface HistoryDao {
    List<Map<String,Object>> selectAll();
    int insert(History history);

    List selectOldAll();

    void update(History history);
}
