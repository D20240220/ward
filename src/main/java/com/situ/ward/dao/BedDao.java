package com.situ.ward.dao;

import com.situ.ward.entity.Bed;
import com.situ.ward.entity.History;
import com.situ.ward.entity.Ward;

import java.util.List;
import java.util.Map;

public interface BedDao {
    List<Map<String,Object>> selectAll();
    List<Bed> selectByBed(Integer id);

    void historyInsert(Integer status,Integer bedId);

    void chuyuan(History history);

    void zhuyuan(History history);

    List getByDeptIdAndStatus(Integer id);
}
