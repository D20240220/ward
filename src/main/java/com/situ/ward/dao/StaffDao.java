package com.situ.ward.dao;

import com.situ.ward.entity.Staff;
import com.situ.ward.entity.Ward;
import com.situ.ward.util.PageInfo;

import java.util.List;
import java.util.Map;

public interface StaffDao {
    int insert(Staff staff);

    int update(Staff staff);

    Staff selectById(Integer id);

    Staff selectByPhone(String phone);
    List<Staff> selectByDept(Integer id);

    List<Map<String,Object>> selectAll(Staff staff);

    PageInfo selectForpage(Integer page, Integer limit, Staff staff);
}
