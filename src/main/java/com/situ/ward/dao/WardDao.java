package com.situ.ward.dao;

import com.situ.ward.entity.Dept;
import com.situ.ward.entity.Ward;

import java.util.List;
import java.util.Map;

public interface WardDao {
    int insert(Ward ward);
    int update(Ward ward);
    List<Ward> selectByDept(Integer id);

    //Dept selectById(Integer id);

    List<Map<String,Object>> selectAll();
}
