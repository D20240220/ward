package com.situ.ward.dao.impl;

import com.situ.ward.dao.HistoryDao;
import com.situ.ward.entity.Dept;
import com.situ.ward.entity.History;
import com.situ.ward.entity.Ward;
import com.situ.ward.util.DBUtil;
import com.sun.org.apache.xalan.internal.xsltc.compiler.Template;

import java.util.List;
import java.util.Map;

public class HistoryDaoImpl implements HistoryDao {
    private DBUtil dbUtil = new DBUtil();


    @Override
    public List<Map<String, Object>> selectAll() {
        String sql = "SELECT history.*,bed.bed ,staff.name staffname,dept.name deptname,ward.name ward from history " +
                "INNER JOIN bed on bed_id=bed.id " +
                "INNER JOIN staff on staff.id=staff_id " +
                "INNER JOIN dept on dept.id=staff.dept_id " +
                "INNER JOIN ward on ward.id=bed.ward_id " +
                "where history.outtime IS NULL";
        return dbUtil.query(sql);
    }
    @Override
    public int insert(History history) {
        String sql="insert into history "+
                "  (id_card, name,bed_id,staff_id,intime)"+
                " values "+
                " ( ?, ?, ?, ?, ?)";
        return dbUtil.update(sql,
                history.getId_card(),history.getName(),
                history.getBed_id(),history.getStaff_id(),
                history.getIntime());

    }

    @Override
    public List selectOldAll() {
        String sql = "SELECT history.*,bed.bed ,staff.name staffname,dept.name deptname,ward.name ward from history " +
                "INNER JOIN bed on bed_id=bed.id " +
                "INNER JOIN staff on staff.id=staff_id " +
                "INNER JOIN dept on dept.id=staff.dept_id " +
                "INNER JOIN ward on ward.id=bed.ward_id " +
                "where history.outtime IS NOT NULL";
        return dbUtil.query(sql);
    }

    @Override
    public void update(History history) {

        String sql = "update history set outtime = ? where id = ?";
        dbUtil.update(sql,history.getOuttime(),history.getId());

    }

}
