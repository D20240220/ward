package com.situ.ward.dao.impl;

import com.situ.ward.dao.DeptDao;
import com.situ.ward.entity.Dept;

import com.situ.ward.util.DBUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class DeptDaoImpl implements DeptDao {

    private DBUtil dbUtil = new DBUtil();
    @Override
    public int insert(Dept dept) {
        String sql="insert into dept "+
                "  (name, room, phone, email, status)"+
                " values "+
                " ( ?, ?, ?, ?, ?)";
        return dbUtil.update(sql,
                dept.getName(),
                dept.getRoom(),dept.getPhone(),
                dept.getEmail(),dept.getStatus());

    }

    @Override
    public int update(Dept dept) {
        String sql = "update dept " +
                "set ";
        List<Object> params = new ArrayList<>();
        if (dept.getName() != null ) {
            sql += "name=?,";
            params.add(dept.getName());
        }
        if (dept.getRoom() != null ) {
            sql += "room=?,";
            params.add(dept.getRoom());
        }
        if (dept.getPhone()!=null&&dept.getPhone().length()>0){
            sql+="phone=?,";
            params.add(dept.getPhone());
        }
        if (dept.getEmail()!=null&&dept.getEmail().length()>0){
            sql+="email=?,";
            params.add(dept.getEmail());
        }
        if (dept.getEmail()!=null&&dept.getEmail().length()>0){
            sql+="status=?,";
            params.add(dept.getStatus());
        }

            sql = sql.substring(0, sql.length() - 1);
            sql += " where id =?";
            params.add(dept.getId());
            System.out.println(sql);
            return dbUtil.update(sql,
                    params.toArray());
    }

    @Override
    public Dept selectById(Integer id) {
        String sql = "select * from dept where id = ?" ;
        return dbUtil.queryOne(Dept.class, sql, id);
    }

    @Override
    public List<Map<String, Object>> selectAll() {
        String sql = "select * from dept ";
        return dbUtil.query(sql);
    }


}
