package com.situ.ward.dao.impl;

import com.situ.ward.dao.StaffDao;
import com.situ.ward.entity.Staff;
import com.situ.ward.util.DBUtil;
import com.situ.ward.util.PageInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class StaffDaoImpl implements StaffDao {
    private DBUtil dbUtil = new DBUtil();
    @Override
    public int insert(Staff staff) {
        String sql="insert into staff "+
                "(password, name, sex, phone, email, role, dept_id) "+
                "values "+
                "(?, ?, ?, ?, ?, ?, ?)";
        return dbUtil.update(sql,
                staff.getPassword(), staff.getName(),
                staff.getSex(), staff.getPhone(),
                staff.getEmail(), staff.getRole(), staff.getDept_id());
    }
    @Override
    public List<Staff> selectByDept(Integer id) {
        String sql = "select * from staff where dept_id = ?" ;
        return dbUtil.query(Staff.class, sql, id);
    }


    @Override
    public int update(Staff staff) {
        String sql = "update staff "+
                "set ";
        List<Object> params = new ArrayList<>();
        if (staff.getPassword() != null && staff.getPassword().length() >0 ){
            sql += "password=?,";
            params.add(staff.getPassword());
        }
        if (staff.getName()!=null&&staff.getName().length()>0){
            sql+="name=?,";
            params.add(staff.getName());
        }
        if (staff.getSex()!=null&&staff.getSex().length()>0){
            sql+="sex=?,";
            params.add(staff.getSex());
        }
        if (staff.getPhone()!=null&&staff.getPhone().length()>0){
            sql+="phone=?,";
            params.add(staff.getPhone());
        }
        if (staff.getEmail()!=null&&staff.getEmail().length()>0){
            sql+="email=?,";
            params.add(staff.getEmail());
        }
        if (staff.getHead()!=null&&staff.getHead().length()>0){
            sql+="head=?,";
            params.add(staff.getHead());
        }
        if (staff.getRole()!=null){
            sql+="role=?,";
            params.add(staff.getRole());
        }
        if (staff.getStatus()!=null){
            sql+="status=?,";
            params.add(staff.getStatus());
        }
        if (staff.getDept_id()!=null){
            sql+="dept_id=?,";
            params.add(staff.getDept_id());
        }
        sql=sql.substring(0,sql.length()-1);
        sql+=" where id =?";
        params.add(staff.getId());
        System.out.println(sql);
        return dbUtil.update(sql,
                params.toArray());
    }

    @Override
    public Staff selectById(Integer id) {
        String sql = "select * from staff where id=?";
        return dbUtil.queryOne(Staff.class,sql,id);
    }

    @Override
    public Staff selectByPhone(String phone) {
        String sql = "select * from staff where phone=?";
        return dbUtil.queryOne(Staff.class,sql,phone);
    }

    @Override
    public List<Map<String,Object>> selectAll(Staff staff) {
        String sql = "select staff.*, dept.name deptName " +
                "from staff " +
                "inner join dept on staff.dept_id=dept.id " +
                "where 1=1 ";
        List<Object> params = new ArrayList<>();
        if (staff.getName() !=null&& staff.getName().length()>0) {
            sql += "and name like ? ";
            params.add("%"+staff.getName()+"%");
        }
        if (staff.getSex() !=null && staff.getSex().length()>0) {
            sql += "and sex = ? ";
            params.add(staff.getSex());
        }
        if (staff.getRole() !=null) {
            sql += "and role = ? ";
            params.add(staff.getRole());
        }
        if (staff.getStatus() !=null) {
            sql += "and status = ? ";
            params.add(staff.getStatus());
        }
        if (staff.getDept_id() !=null) {
            sql += "and dept_id = ? ";
            params.add(staff.getDept_id());
        }

        return dbUtil.query(sql,params.toArray());
    }


    @Override
    public PageInfo selectForpage(Integer page, Integer limit, Staff staff) {
        String sqlWhere="";
        List<Object> params = new ArrayList<>();
        if (staff.getName()!=null && staff.getName().length()>0) {
            sqlWhere += "and staff.name like ? ";
            params.add("%"+staff.getName()+"%");
        }
        if (staff.getSex()!=null && staff.getSex().length()>0) {
            sqlWhere += "and sex = ? ";
            params.add(staff.getSex());
        }
        if (staff.getRole()!=null) {
            sqlWhere += "and role = ? ";
            params.add(staff.getRole());
        }
        if (staff.getStatus()!=null) {
            sqlWhere += "and staff.status = ? ";
            params.add(staff.getStatus());
        }
        if (staff.getDept_id()!=null) {
            sqlWhere += "and dept_id = ? ";
            params.add(staff.getDept_id());
        }
        //记录的条数
        String sql = "select count(*) count " +
                "from staff " +
                "where 1=1 " +
                sqlWhere;
        List<Map<String,Object>> list1= dbUtil.query(sql,params.toArray());
        Long count= (Long) list1.get(0).get("count");
        //当前页要显示的数据
        sql = "select staff.* ,dept.name deptName " +
                "from staff " +
                "inner join dept on staff.dept_id=dept.id " +
                "where 1=1 "+sqlWhere+
                " limit ?,? ";
        params.add((page-1)*limit);
        params.add(limit);


        List list= dbUtil.query(sql,params.toArray());

        PageInfo pageInfo = new PageInfo(count,list);

        return pageInfo;
    }
}
