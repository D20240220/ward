package com.situ.ward.dao.impl;

import com.situ.ward.dao.BedDao;
import com.situ.ward.entity.Bed;
import com.situ.ward.entity.History;
import com.situ.ward.entity.Ward;
import com.situ.ward.util.DBUtil;

import java.util.List;
import java.util.Map;

public class BedDaoImpl implements BedDao {
    private DBUtil dbUtil = new DBUtil();
    @Override
    public List<Map<String, Object>> selectAll() {
        String sql = "SELECT bed.*,name FROM bed " +
                " INNER JOIN ward ON bed.ward_id = ward.id";
        return dbUtil.query(sql);
    }

    @Override
    public List<Bed> selectByBed(Integer id) {
        String sql = "select * from bed where ward_id = ?" ;
        return dbUtil.query(Bed.class, sql, id);
    }

    @Override
    public void historyInsert(Integer status,Integer bedId) {
        String sql = "update bed set status = 1 where id = ?";
        dbUtil.update(sql,bedId);
    }

    @Override
    public void chuyuan(History history) {
        String sql = "update bed set status = 0 where bed = ?";
        dbUtil.update(sql,history.getBed());
    }

    @Override
    public void zhuyuan(History history) {
        String sql = "update bed set status = 1 where id = ?";
        dbUtil.update(sql,history.getId());
    }

    @Override
    public List getByDeptIdAndStatus(Integer id) {
        String sql = "select * from bed where status = 0 and ward_id = ?" ;
        return dbUtil.query(Bed.class, sql, id);
    }


}
