package com.situ.ward.dao.impl;

import com.situ.ward.dao.WardDao;
import com.situ.ward.entity.Dept;
import com.situ.ward.entity.Ward;
import com.situ.ward.util.DBUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class WardDaoImpl implements WardDao {
    private DBUtil dbUtil = new DBUtil();
    @Override
    public int insert(Ward ward) {
        String sql="insert into ward "+
                "  (name, dept_id)"+
                " values "+
                " ( ?, ?)";
        return dbUtil.update(sql,
                ward.getName(),
                ward.getDept_id());
    }

    @Override
    public int update(Ward ward) {
        String sql = "update ward " +
                "set ";
        List<Object> params = new ArrayList<>();
        if (ward.getName() != null ) {
            sql += "name=?,";
            params.add(ward.getName());
        }
        if (ward.getName() != null ) {
            sql += "name=?,";
            params.add(ward.getName());
        }


        sql = sql.substring(0, sql.length() - 1);
        sql += " where id =?";
        params.add(ward.getId());
        System.out.println(sql);
        return dbUtil.update(sql,
                params.toArray());
    }

//    @Override
//    public Dept selectById(Integer id) {
//        String sql = "select * from dept where id = ?" ;
//        return dbUtil.queryOne(Dept.class, sql, id);
//    }

    @Override
    public List<Map<String, Object>> selectAll() {
        String sql = "select ward.*, dept.`name` dept_name from ward\n" +
                "left join dept on ward.dept_id = dept.id";
        return dbUtil.query(sql);
    }
    @Override
    public List<Ward> selectByDept(Integer id) {
        String sql = "select * from ward where dept_id = ?" ;
        return dbUtil.query(Ward.class, sql, id);
    }
}
