package com.situ.ward.dao;

import com.situ.ward.entity.Dept;
import com.situ.ward.entity.Staff;

import java.util.List;
import java.util.Map;

public interface DeptDao {
    int insert(Dept dept);

    int update(Dept dept);

    Dept selectById(Integer id);

    List<Map<String,Object>> selectAll();



}
