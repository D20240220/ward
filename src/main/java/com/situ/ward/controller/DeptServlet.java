package com.situ.ward.controller;

import com.situ.ward.entity.Dept;
import com.situ.ward.entity.Result;
import com.situ.ward.entity.Staff;
import com.situ.ward.service.DeptService;
import com.situ.ward.service.impl.DeptServiceImpl;
import com.situ.ward.util.BaseServlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "DeptServlet", value = "/dept")
public class DeptServlet extends BaseServlet {
    private DeptService deptService = new DeptServiceImpl();
    public void search(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        List depts = deptService.getAll();
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().append(Result.ok(depts).toJson());

    }
    public void remove(HttpServletRequest request,HttpServletResponse response)
            throws ServletException,IOException{
        //1.获取参数
        String sId=request.getParameter("id");
        try {
            Integer id = Integer.parseInt(sId);

            //2.调用Servioce
            deptService.remove(id);

            //3.返回数据
            response.setContentType("application/json;charset=UTF-8");
            response.getWriter().append(Result.ok().toJson());
        }catch (Exception e){
            e.printStackTrace();

            response.setContentType("application/json;charset=UTF-8");
            response.getWriter().append(Result.error(e.getMessage()).toJson());
        }
    }

    public void getById(HttpServletRequest request,HttpServletResponse response)
            throws ServletException,IOException{
        //1-获取参数
        String sId = request.getParameter("id");
        Integer id = Integer.parseInt(sId);
        //2-调用Service
        Dept dept = deptService.getById(id);
        //3-返回数据
        response.setContentType( "application/json; charset=UTF-8");
        response.getWriter().append(Result.ok(dept).toJson());

    }
    //修改部门
    public void edit(HttpServletRequest request,HttpServletResponse response)
            throws ServletException,IOException{
        //1.获取参数

        String sId = request.getParameter( "id" );
        String name = request.getParameter( "name" );
        String room = request.getParameter("room");
        String phone = request.getParameter(  "phone");
        String email = request.getParameter(  "email");
        //String sStatus = request.getParameter(  "status");


        Integer id = Integer.parseInt(sId);
        //Integer status = Integer.parseInt(sStatus);

        Dept dept = new Dept();
        dept.setId (id);
        dept.setName (name);
        dept.setRoom(room);
        dept.setPhone(phone) ;
        dept.setEmail(email);
        //dept.setStatus(status);

        try {
            deptService.edit(dept);
            response.setContentType( "application/json; charset=UTF-8");
            response.getWriter().append(Result.ok().toJson());
        } catch (Exception e) {
            e.printStackTrace();
            response.setContentType( "application/json; charset=UTF-8");
            response.getWriter().append(Result.error(e.getMessage()).toJson());
        }

    }
    public void add(HttpServletRequest request,HttpServletResponse response)
            throws ServletException,IOException{
        String name = request.getParameter( "name" );
        String room = request.getParameter("room");
        String phone = request.getParameter(  "phone");
        String email = request.getParameter(  "email");

        Dept dept = new Dept();
        dept.setName (name);
        dept.setRoom (room);
        dept.setPhone (phone);
        dept.setEmail (email);
        dept.setStatus(0);


        try {
            deptService.add(dept);
            response.setContentType( "application/json; charset=UTF-8");
            response.getWriter().append(Result.ok().toJson());
        } catch (Exception e) {
            e.printStackTrace();
            response.setContentType( "application/json; charset=UTF-8");
            response.getWriter().append(Result.error(e.getMessage()).toJson());
        }
    }


}
