package com.situ.ward.controller;

import com.situ.ward.util.VerCodeUtil;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "VerCodeServlet", value = "/verCode")
public class VerCodeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //验证码，回头再补   verCode,生成的在session中
        String verCode = VerCodeUtil.createVerCode(response);
        request.getSession().setAttribute("verCode",verCode);


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }
}
