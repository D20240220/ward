package com.situ.ward.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.File;
import java.io.IOException;

@WebServlet(name = "UploadServlet", value = "/upload")
@MultipartConfig
public class UploadServlet extends HttpServlet {
    private static final String PATH = "D:/upload";
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String head = request.getParameter("head");
//        com.situ.expense.util.FileUploadUtil.download(head,PATH,response);
        com.situ.expense.util.FileUploadUtil.download(head,PATH,response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //上传图片
        //从request中获取文件
        Part file = request.getPart("file");
        String fileName = com.situ.expense.util.FileUploadUtil.upload(file,PATH);
        //返回JSON的格式
        response.setContentType("application/json; charset=UTF-8");
        response.getWriter().append("{\"code\":0,\"msg\":\""+fileName+"\"}");

    }
}
