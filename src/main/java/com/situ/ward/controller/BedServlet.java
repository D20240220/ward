package com.situ.ward.controller;

import com.situ.ward.entity.Result;
import com.situ.ward.service.BedService;
import com.situ.ward.service.WardService;
import com.situ.ward.service.impl.BedServiceImpl;
import com.situ.ward.service.impl.WardServiceImpl;
import com.situ.ward.util.BaseServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "BedServlet", value = "/bed")
public class BedServlet extends BaseServlet {
    private BedService bedService = new BedServiceImpl();
    public void search(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List beds = bedService.getAll();
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().append(Result.ok(beds).toJson());

    }


    public void search1 (HttpServletRequest request,HttpServletResponse response)
            throws ServletException,IOException {
        Integer sId = Integer.valueOf((request.getParameter(  "wardId" )));

        List beds = bedService.getByDeptIdAndStatus(sId);
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().append(Result.ok(beds).toJson());
    }

}
