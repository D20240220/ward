package com.situ.ward.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.situ.ward.entity.Result;
import com.situ.ward.entity.Staff;
import com.situ.ward.service.StaffService;
import com.situ.ward.service.impl.StaffServiceImpl;
import com.situ.ward.util.BaseServlet;
import com.situ.ward.util.PageInfo;
import org.springframework.util.ObjectUtils;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.EOFException;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executors;

@WebServlet(name = "StaffServlet", value = "/staff")
public class StaffServlet extends BaseServlet {


    private StaffService staffService = new StaffServiceImpl();

    public void search1 (HttpServletRequest request,HttpServletResponse response)
            throws ServletException,IOException {
        Integer sId = Integer.valueOf((request.getParameter(  "deptId" )));

        List staffs = staffService.getByDeptId(sId);
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().append(Result.ok(staffs).toJson());
    }
    ///staff？action=login
    public void logout(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //从Session中清除登陆状态
        request.getSession().invalidate();
        //请求转发还是重定向？？ 优先重定向
        response.sendRedirect(request.getContextPath()+"/staff/login.jsp");

    }
    ///staff？action=login
    public void login(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //1.获取参数
        String sId = request.getParameter("id");
        String password = request.getParameter("password");
        String verCode = request.getParameter("verCode");
        Integer id = null;
        try {
            try {
                id = Integer.parseInt(sId);
            } catch (NumberFormatException e) {
                throw new Exception("工号不合法");
            }
            // 2．验证参数
            if (password == null ||
                    password.length() < 3 ||
                    password.length() > 16){
                throw new Exception("密码格式不对! ");
            }
            //验证码，回头再补
//            String sVerCode = (String)request.getSession().getAttribute("verCode");
//            if (sVerCode == null||!sVerCode.equalsIgnoreCase(verCode)){
//                throw new Exception("验证码错误");
//            }
            Staff staff = new Staff();
            staff.setId(id);
            staff.setPassword(password);
            //调用Service层
            staff = staffService.login(staff);

            String autoLogin = request.getParameter("autoLogin");
            if (!ObjectUtils.isEmpty(autoLogin)) {
                Cookie cookie = new Cookie("autoLogin", staff.getName() + "&" + staff.getPassword());
                //设置Cookie 的存活时间和绑定路径
                cookie.setMaxAge(60*60*24*7);//设置时间为1周
                cookie.setPath(request.getContextPath());
                //在响应resp中添加cookie，返回给浏览器
                response.addCookie(cookie);
            }
            //保存到session中
            request.getSession().setAttribute("staff", staff);
            //跳转到主页面
            response.sendRedirect(request.getContextPath()+"/staff/index.jsp");
        }catch (Exception e){
            e.printStackTrace();
            //登录失败拉
            request.setAttribute("error", e.getMessage());//返回登录页面
            request.getRequestDispatcher("/staff/login.jsp" ).forward(request,response);
        }

    }
    //修改员工
    public void resetPwd(HttpServletRequest request,HttpServletResponse response)
        throws ServletException,IOException{
        //1.获取参数
        String oldPwd = request.getParameter("oldPwd");
        String newPwd = request.getParameter("newPwd");
        String rePwd = request.getParameter("rePwd");
        //2.校验参数
        try {
            if (oldPwd == null || !oldPwd.matches("^.{3,16}$")) {
                throw new Exception("旧密码格式不正确");
            }
            if (newPwd == null || !newPwd.matches("^.{3,16}$")) {
                throw new Exception("新密码格式不正确");
            }
            if (rePwd == null || !rePwd.equals(newPwd)) {
                throw new Exception("两次密码不一致");
            }
            //获取当前登陆的用户   员工没登录呢？？？？
            Staff staff = (Staff) request.getSession().getAttribute("staff");
            //3.调用Service
            staffService.resetPwd(oldPwd,newPwd,staff.getId());
            //修改成功
            request.getSession().invalidate();
            //返回JSON     成功
            response.setContentType("application/json; charset=UTF-8");
            response.getWriter().append("{\"code\":0}");

        }catch (Exception e){
            e.printStackTrace();
            //修改失败
            //返回JSON     失败，错误信息
            response.setContentType("application/json;charset=UTF-8");
            response.getWriter().append("{\"code\": 1, \"msg\":\""+e.getMessage()+"\"}");
        }
    }
    public void modify(HttpServletRequest request,HttpServletResponse response)
        throws ServletException,IOException{

        //1.获取参数
        String phone = request.getParameter("phone");
        String email = request.getParameter("email");
        String head = request.getParameter("head");

        //2.参数校验
        try {
            if (phone == null || !phone.matches("^1[35-9][0-9]{9}$")){
                throw new Exception("电话格式错误!");
            }
            if (email == null || !email.matches("^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*.\\w+([-.]\\w+)*$")){
                throw new Exception("邮箱格式错误!");
            }

            //3.调用Service
            Staff lStaff = (Staff) request.getSession().getAttribute("staff");
            Staff staff = new Staff();
            staff.setPhone(phone);
            staff.setEmail(email);
            staff.setHead(head);
            staff.setId(lStaff.getId());

            staffService.edit(staff);

            //修改成功  更新一下当前登录员工信息
            lStaff = staffService.getById(lStaff.getId());
            request.getSession().setAttribute("staff",lStaff);
            response.setContentType( "application/json; charset=UTF-8");
            response.getWriter().append("{\"code\":0}");

        }catch (Exception e){
            e.printStackTrace();

            response.setContentType("application/json; charset=UTF-8");
            response.getWriter().append("{\"code\":1,\"msg\":\""+e.getMessage()+"\"]");

        }




    }
    //分页查询
    public void search (HttpServletRequest request,HttpServletResponse response)
        throws ServletException,IOException{
        //1-获取参数page limit
        String sPage = request.getParameter(  "page" );
        String sLimit = request.getParameter( "limit");
        String name = request.getParameter( "name");
        String sex = request.getParameter(  "sex");
        String sRole = request.getParameter(  "role");
        String sStatus = request.getParameter(  "status");
        String sDeptId = request.getParameter("dept_id");

        Staff staff = new Staff();
        staff.setName(name);
        staff.setSex(sex);
        if (sRole!=null && sRole.length()>0){
            staff.setRole(Integer.parseInt(sRole));
        }
        if (sStatus!=null && sStatus.length()>0){
            staff.setStatus(Integer.parseInt(sStatus));
        }
        if (sDeptId!=null && sDeptId.length()>0){
            staff.setDept_id(Integer.parseInt(sDeptId));
        }
        Integer page = null,limit = null;
        try {
            page = Integer.parseInt(sPage);
            limit = Integer.parseInt(sLimit);
        }catch (Exception e){}

        //是否分页
        if (page != null ) {
            //分页
            PageInfo pageInfo = staffService.getForPage(page,limit,staff);
            response.setContentType("application/json; charset=UTF-8");
            response.getWriter().append(Result.ok(pageInfo).toJson());
        }else {
            List staffs = staffService.getAll(staff);
            response.setContentType("application/json; charset=UTF-8");
            response.getWriter().append(Result.ok(staffs).toJson());
        }
    }
    public void remove(HttpServletRequest request,HttpServletResponse response)
        throws ServletException,IOException{
        //1.获取参数
        String sId=request.getParameter("id");
        try {
            Integer id = Integer.parseInt(sId);

            //2.调用Servioce
            staffService.remove(id);

            //3.返回数据
            response.setContentType("application/json;charset=UTF-8");
            response.getWriter().append(Result.ok().toJson());
        }catch (Exception e){
            e.printStackTrace();

            response.setContentType("application/json;charset=UTF-8");
            response.getWriter().append(Result.error(e.getMessage()).toJson());
        }
    }
    public void add(HttpServletRequest request,HttpServletResponse response)
        throws ServletException,IOException{
        String name = request.getParameter( "name" );
        String sex = request.getParameter("sex");
        String phone = request.getParameter(  "phone");
        String email = request.getParameter(  "email");
        String sRole = request.getParameter(  "role");
        String sDeptId = request.getParameter(  "dept_id");

        Integer role = Integer.parseInt(sRole);
        Integer deptId = Integer.parseInt(sDeptId);

        Staff staff = new Staff();
        staff.setName (name);
        staff.setSex(sex);
        staff.setPhone(phone) ;
        staff.setEmail(email);
        staff.setRole(role);
        staff.setDept_id(deptId);
        staff.setPassword("123");

        try {
            staffService.add(staff);
            response.setContentType( "application/json; charset=UTF-8");
            response.getWriter().append(Result.ok().toJson());
        } catch (Exception e) {
            e.printStackTrace();
            response.setContentType( "application/json; charset=UTF-8");
            response.getWriter().append(Result.error(e.getMessage()).toJson());
        }
    }
    public void getById(HttpServletRequest request,HttpServletResponse response)
        throws ServletException,IOException{
        //1-获取参数
        String sId = request.getParameter("id");
        Integer id = Integer.parseInt(sId);
        //2-调用Service
        Staff staff = staffService.getById(id);
        //3-返回数据
        response.setContentType( "application/json; charset=UTF-8");
        response.getWriter().append(Result.ok(staff).toJson());

    }
    public void edit(HttpServletRequest request,HttpServletResponse response)
            throws ServletException,IOException{
        //1.获取参数

        String sId = request.getParameter( "id" );
        String name = request.getParameter( "name" );
        String sex = request.getParameter("sex");
        String phone = request.getParameter(  "phone");
        String email = request.getParameter(  "email");
        String sRole = request.getParameter(  "role");
        String sDeptId = request.getParameter(  "dept_id");


        Integer id = Integer.parseInt(sId);
        Integer role = Integer.parseInt(sRole);
        Integer deptId = Integer.parseInt(sDeptId);

        Staff staff = new Staff();
        staff.setId (id);
        staff.setName (name);
        staff.setSex(sex);
        staff.setPhone(phone) ;
        staff.setEmail(email);
        staff.setRole(role);
        staff.setDept_id(deptId);

        try {
            staffService.edit(staff);
            response.setContentType( "application/json; charset=UTF-8");
            response.getWriter().append(Result.ok().toJson());
        } catch (Exception e) {
            e.printStackTrace();
            response.setContentType( "application/json; charset=UTF-8");
            response.getWriter().append(Result.error(e.getMessage()).toJson());
        }

    }


}

