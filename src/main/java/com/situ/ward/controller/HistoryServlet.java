package com.situ.ward.controller;

import com.situ.ward.entity.History;
import com.situ.ward.entity.Result;
import com.situ.ward.service.BedService;
import com.situ.ward.service.HistoryService;
import com.situ.ward.service.impl.BedServiceImpl;
import com.situ.ward.service.impl.HistoryServiceImpl;
import com.situ.ward.util.BaseServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.DateFormatter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

@WebServlet(name = "HistoryServlet", value = "/history")
public class HistoryServlet extends BaseServlet {
    private HistoryService historyService = new HistoryServiceImpl();
    private BedService bedService = new BedServiceImpl();

    public void search(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List cases = historyService.getAll();
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().append(Result.ok(cases).toJson());
    }
    public void search1(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List cases = historyService.getOldAll();
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().append(Result.ok(cases).toJson());
    }
    public void add(HttpServletRequest request,HttpServletResponse response)
            throws ServletException,IOException{
        String id_card = request.getParameter( "id_card" );
        String name = request.getParameter( "name" );
        String sBedid = request.getParameter("bed");
        String sStaff_id = request.getParameter(  "staffname");
        String sIn = request.getParameter(  "in");
        Integer bed_id= Integer.valueOf(sBedid);
        Integer staff_id= Integer.valueOf(sStaff_id);

        History history = new History();
        history.setId_card(id_card);
        history.setName(name);
        history.setBed_id(bed_id);
        history.setStaff_id(staff_id);
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        history.setIntime(LocalDateTime.parse(sIn, dateFormat));
        try {
            historyService.add(history);
            bedService.zhuyuan(history);
            response.setContentType( "application/json; charset=UTF-8");
            response.getWriter().append(Result.ok().toJson());
        } catch (Exception e) {
            e.printStackTrace();
            response.setContentType( "application/json; charset=UTF-8");
            response.getWriter().append(Result.error(e.getMessage()).toJson());
        }
    }
    public void remove(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter( "id" );

        String timestampStr = request.getParameter(  "out");
        String ward = request.getParameter( "ward" );
        String bed = request.getParameter( "bed" );

        History history = new History();
        history.setId(Integer.valueOf(id));
        history.setWard(ward);
        history.setBed(bed);

// 将时间戳字符串转换为毫秒数
        long timestamp = Long.parseLong(timestampStr);
// 将毫秒数转换为 LocalDateTime 对象
        Instant instant = Instant.ofEpochMilli(timestamp);
        LocalDateTime dateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());

// 使用指定的格式进行格式化
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formattedDateTime = dateTime.format(dateFormat);

// 将格式化后的日期时间字符串解析为 LocalDateTime 对象
        LocalDateTime parsedDateTime = LocalDateTime.parse(formattedDateTime, dateFormat);
        System.out.println(parsedDateTime);
// 将解析后的 LocalDateTime 对象设置给 history 的 intime 属性
        history.setOuttime(parsedDateTime);
        try {
            historyService.chuyuan(history);
            bedService.chuyuan(history);
            response.setContentType( "application/json; charset=UTF-8");
            response.getWriter().append(Result.ok().toJson());
        } catch (Exception e) {
            e.printStackTrace();
            response.setContentType( "application/json; charset=UTF-8");
            response.getWriter().append(Result.error(e.getMessage()).toJson());
        }
    }

}
