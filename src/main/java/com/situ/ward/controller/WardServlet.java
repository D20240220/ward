package com.situ.ward.controller;

import com.situ.ward.entity.Result;
import com.situ.ward.entity.Staff;
import com.situ.ward.service.WardService;
import com.situ.ward.service.impl.WardServiceImpl;
import com.situ.ward.util.BaseServlet;
import com.situ.ward.util.PageInfo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@WebServlet(name = "WardServlet", value = "/ward")
public class WardServlet extends BaseServlet {
    private WardService wardService = new WardServiceImpl();
    public void search(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List wards = wardService.getAll();
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().append(Result.ok(wards).toJson());

    }
    public void search1 (HttpServletRequest request,HttpServletResponse response)
            throws ServletException,IOException {
        Integer sId = Integer.valueOf((request.getParameter(  "deptId" )));

        List wards = wardService.getByDeptId(sId);
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().append(Result.ok(wards).toJson());
    }
}
