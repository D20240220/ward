package com.situ.ward.vo;

import com.situ.ward.entity.Staff;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
public class StaffVo extends Staff {
    private String deptName;
}
