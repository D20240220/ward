package com.situ.expense.util;

import java.io.*;
import java.util.UUID;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 * 文件上传
 * Servlet前需要添加注解 @MultipartConfig
 
 * @author bobzyh 
 */
public class FileUploadUtil {

	public static void download(String fileName, String path, HttpServletResponse response){
		// IO流
		try (
				FileInputStream in = new FileInputStream(path + "/" + fileName);
				OutputStream out = response.getOutputStream();
		){
			byte[] buff = new byte[1024];

			while (true) {
				int length = in.read(buff);
				if (length < 0) {
					break;
				}
				out.write(buff, 0, length);
			}

			out.flush();
		} catch (Exception e){
			e.printStackTrace();
			throw new RuntimeException("文件读取失败");
		}
	}

	/**
	 * @param part	文件
	 * @param path	保存路径
	 * @return 保存成功返回文件名, 保存失败返回null
	 */
	public static String upload(Part part, String path) {
		String disposition = part.getHeader("Content-Disposition");
		String suffix = disposition.substring(disposition.lastIndexOf("."), disposition.length() - 1);
		// 随机的生存一个32的字符串
		String filename = UUID.randomUUID() + suffix;

		File file = new File(path);
		if (!file.exists()) {
			file.mkdirs();
		}

		// 获取上传的文件名
		InputStream is = null;
		FileOutputStream fos = null;
		try {
			is = part.getInputStream();
			fos = new FileOutputStream(path + "/" + filename);
			byte[] bty = new byte[1024];
			int length = 0;
			while ((length = is.read(bty)) != -1) {
				fos.write(bty, 0, length);
			}

			return filename;
		} catch (IOException e) {
			e.printStackTrace();
		} finally {

			try {
				if (fos != null)
					fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				if (is != null)
					is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return null;
	}
}
