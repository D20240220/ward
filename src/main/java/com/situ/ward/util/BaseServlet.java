package com.situ.ward.util;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 通用的Servlet,将所有请求，根据action参数进行分发
 */
public abstract class BaseServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        response.setContentType("text/html; charset=utf-8");
        // 反射
        try {
            if (action != null) {
                Method method = this.getClass().getDeclaredMethod(
                        action,
                        HttpServletRequest.class,
                        HttpServletResponse.class);
                // 找到这个方法了
                // 调用它
                method.invoke(this, request, response);
            } else {
                // 没有这个方法
                response.getWriter().append("你没有传action参数啊，亲~~~");
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            // 没有这个方法
            response.getWriter().append("当前类没有实现对应的方法");
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
