package com.situ.ward.util;

import org.springframework.util.ObjectUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class CookieUtils {

    public static Cookie getCookie(HttpServletRequest request, String cookieName) {
        Cookie[] cookies = request.getCookies();
        Cookie cookie = null;
        if (!ObjectUtils.isEmpty(cookies)) {
            //遍历cookies
            for (Cookie c : cookies) {
                String name = c.getName();
                if (cookieName.equals(name)) {
                    //说明cookie中存在自动登录
                    cookie = c;
                    break;
                }
            }
        }
        return cookie;
    }
}
