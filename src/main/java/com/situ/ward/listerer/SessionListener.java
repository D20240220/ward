package com.situ.ward.listerer;

import com.situ.ward.entity.Staff;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.util.HashSet;
import java.util.Set;

/**
 *
 */
@WebListener
public class SessionListener implements
    //Request
//    ServletRequestListener,                     //生命周期
//    ServletRequestAttributeListener,            //属性变化
    //Session
    HttpSessionListener,                        //生命周期
    HttpSessionAttributeListener               //属性变化
    //application
    //
{
    @Override
    public void sessionCreated(HttpSessionEvent se) {
        //当一个session对象被创建时，该方法会被调用

        //记录当前访问的用户
        ServletContext application= se.getSession().getServletContext();
        Integer onlineCount = (Integer) application.getAttribute("onlineCount");
        if (onlineCount==null){
            onlineCount=0;
        }
        onlineCount++;
        //写回去
        application.setAttribute("onlineCount",onlineCount);

    }

    @Override
    public void attributeAdded(HttpSessionBindingEvent event) {
        //当前登录用户都有谁
        if ("staff".equals(event.getName())){
            //新登录了一个用户
            Staff staff = (Staff) event.getValue();
            //application对象
            ServletContext application = event.getSession().getServletContext();
            Set<Staff> loginUsers = (Set<Staff>) application.getAttribute("loginUsers");
            if (loginUsers==null){
                loginUsers=new HashSet<>();
            }
            loginUsers.add(staff);
            application.setAttribute("loginUsers",loginUsers);

        }
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent event) {
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent event) {

    }
}



