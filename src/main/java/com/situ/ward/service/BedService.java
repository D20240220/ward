package com.situ.ward.service;

import com.situ.ward.entity.History;

import java.util.List;

public interface BedService {
    List getAll();
    List getByDeptId(Integer id);

    void chuyuan(History history);

    void zhuyuan(History history);
    List getByDeptIdAndStatus(Integer id);
}
