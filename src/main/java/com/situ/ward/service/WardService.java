package com.situ.ward.service;

import com.situ.ward.entity.Staff;
import com.situ.ward.entity.Ward;

import java.util.List;

public interface WardService {
    List getAll();

    List getByDeptId(Integer id);
}
