package com.situ.ward.service.impl;

import com.situ.ward.dao.WardDao;
import com.situ.ward.dao.impl.WardDaoImpl;
import com.situ.ward.entity.Staff;
import com.situ.ward.entity.Ward;
import com.situ.ward.service.WardService;

import java.util.List;

public class WardServiceImpl implements WardService {
    private WardDao wardDao = new WardDaoImpl();

    @Override
    public List getAll() {
        return wardDao.selectAll();
    }



    @Override
    public List getByDeptId(Integer id) {
        return wardDao.selectByDept(id);
    }
}
