package com.situ.ward.service.impl;

import com.situ.ward.dao.BedDao;
import com.situ.ward.dao.impl.BedDaoImpl;
import com.situ.ward.entity.History;
import com.situ.ward.service.BedService;

import java.util.List;

public class BedServiceImpl implements BedService {
    private BedDao bedDao = new BedDaoImpl();
    @Override
    public List getAll() {
        return bedDao.selectAll();
    }

    @Override
    public List getByDeptId(Integer id) {
        return bedDao.selectByBed(id);
    }
    @Override
    public List getByDeptIdAndStatus(Integer id) {
        return bedDao.getByDeptIdAndStatus(id);
    }

    @Override
    public void chuyuan(History history) {
        bedDao.chuyuan(history);
    }

    @Override
    public void zhuyuan(History history) {
        bedDao.zhuyuan(history);
    }
}
