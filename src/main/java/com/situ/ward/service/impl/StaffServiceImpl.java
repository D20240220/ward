package com.situ.ward.service.impl;

import com.situ.ward.dao.StaffDao;
import com.situ.ward.dao.impl.StaffDaoImpl;
import com.situ.ward.entity.Staff;
import com.situ.ward.service.StaffService;
import com.situ.ward.util.MD5Util;
import com.situ.ward.util.PageInfo;

import java.util.List;

/**
 * service层的实现层
 */
public class StaffServiceImpl  implements StaffService {
    private StaffDao staffDao = new StaffDaoImpl();
    private static final String SALT="abcd";
    @Override
    public int add(Staff staff) throws Exception {
        /**
         * 保证唯一字段，  电话不能重复
         */
        Staff sStaff = staffDao.selectByPhone(staff.getPhone());
        if (sStaff!=null){
            throw new Exception("电话已被使用");
        }
        staff.setPassword(MD5Util.getDBMD5(staff.getPassword(),SALT));
        return staffDao.insert(staff);
    }

    @Override
    public int remove(Integer id) throws Exception {
        //软删除  将status从0变成1
        Staff staff = staffDao.selectById(id);
        if (staff==null){
            throw new Exception("员工不存在");
        }
        if (staff.getStatus()==1){
            throw new Exception("员工已离职");
        }
        staff.setStatus(1);
        return staffDao.update(staff);
    }

    @Override
    public int edit(Staff staff) throws Exception {
        Staff sStaff = staffDao.selectByPhone(staff.getPhone());
        if (sStaff!=null&&!sStaff.getId().equals(staff.getId())){
            throw new Exception("电话已被使用");
        }
        return staffDao.update(staff);
    }

    @Override
    public Staff login(Staff staff) throws Exception {
        Staff sStaff = staffDao.selectById(staff.getId());
        if (sStaff==null){
            throw new Exception("工号不存在");
        }
        String md5Pwd = MD5Util.getDBMD5(staff.getPassword(),SALT);
        if (!sStaff.getPassword().equals(md5Pwd)){
            throw new Exception("密码错误");
        }
        if (sStaff.getStatus()==1){
            throw new Exception("员工已离职");
        }
        return sStaff;
    }

    @Override
    public Staff getById(Integer id) {
        return staffDao.selectById(id);
    }

    @Override
    public List getAll(Staff staff) {
        return staffDao.selectAll(staff);
    }

    @Override
    public int resetPwd(String oldPwd, String newPwd, Integer id) throws Exception {
        //新密码与旧密码不能一样
        if (oldPwd.equals(newPwd)){
            throw new Exception("新旧密码不能一样");
        }

        //旧密码是否正确
        Staff sStaff = staffDao.selectById(id);
        String md5Pwd = MD5Util.getDBMD5(oldPwd,SALT);
        if (!md5Pwd.equals(sStaff.getPassword())){
            throw new Exception("旧密码错误");
        }
        newPwd = MD5Util.getDBMD5(newPwd,SALT);
        sStaff.setPassword(newPwd);

        return staffDao.update(sStaff);
    }

    @Override
    public PageInfo getForPage(Integer page, Integer limit, Staff staff) {
        return staffDao.selectForpage(page,limit,staff);
    }

    @Override
    public List getByDeptId(Integer sId) {
        return staffDao.selectByDept(sId);
    }
}
