package com.situ.ward.service.impl;

import com.situ.ward.dao.DeptDao;
import com.situ.ward.dao.impl.DeptDaoImpl;
import com.situ.ward.entity.Dept;
import com.situ.ward.entity.Staff;
import com.situ.ward.service.DeptService;

import java.util.List;

public class DeptServiceImpl implements DeptService {
    private DeptDao deptDao = new DeptDaoImpl();
    @Override
    public int add(Dept dept) throws Exception {
        return deptDao.insert(dept);
    }

    @Override
    public int remove(Integer id) throws Exception {
        //软删除  将status从0变成1
        Dept dept = deptDao.selectById(id);
        if (dept==null){
            throw new Exception("部门不存在");
        }
        if (dept.getStatus()==1){
            throw new Exception("部门已离职");
        }
        dept.setStatus(1);
        return deptDao.update(dept);
    }

    @Override
    public int edit(Dept dept) throws Exception {
        return deptDao.update(dept);
    }

    @Override
    public List getAll() {
        return deptDao.selectAll();
    }

    @Override
    public Dept getById(Integer id)  {
        return deptDao.selectById(id);
    }

}
