package com.situ.ward.service.impl;

import com.situ.ward.dao.BedDao;
import com.situ.ward.dao.HistoryDao;
import com.situ.ward.dao.impl.BedDaoImpl;
import com.situ.ward.dao.impl.HistoryDaoImpl;
import com.situ.ward.entity.History;
import com.situ.ward.entity.Staff;
import com.situ.ward.service.HistoryService;
import com.situ.ward.util.MD5Util;

import java.util.List;

public class HistoryServiceImpl implements HistoryService {
    private HistoryDao historyDao = new HistoryDaoImpl();
    private BedDao bedDao = new BedDaoImpl();
    @Override
    public List getAll() {
        return historyDao.selectAll();
    }
    @Override
    public int add(History history) throws Exception {
            bedDao.historyInsert(0,history.getBed_id());
        return historyDao.insert(history);
    }

    @Override
    public List getOldAll() {
        return historyDao.selectOldAll();
    }

    @Override
    public void chuyuan(History history) {
        historyDao.update(history);
    }
}
