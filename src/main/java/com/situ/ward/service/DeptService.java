package com.situ.ward.service;

import com.situ.ward.entity.Dept;

import java.util.List;

public interface DeptService {

    int add(Dept dept) throws Exception;
    int remove(Integer id) throws Exception;
    int edit(Dept dept) throws Exception;
    List getAll();

    Dept getById(Integer id);
}
