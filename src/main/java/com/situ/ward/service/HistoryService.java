package com.situ.ward.service;

import com.situ.ward.entity.History;
import com.situ.ward.entity.Staff;

import java.util.List;

public interface HistoryService {
    List getAll();
    int add(History history) throws Exception;

    List getOldAll();

    void chuyuan(History history);
}
