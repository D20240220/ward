package com.situ.ward.service;

import com.situ.ward.entity.Staff;
import com.situ.ward.util.PageInfo;

import java.util.List;

public interface StaffService {


    /**
     * 添加员工
     * @param staff 员工的所有基础信息
     * @return 添加成功返回证书
     * @throws Exception 添加失败时抛出异常，失败原因在message中
     */
    int add(Staff staff) throws Exception;

    /**
     * 删除员工，逻辑删除只是将员工的状态改为离职
     * @param id 员工的工号
     * @return 删成功时返回整数
     * @throws Exception 删除失败时抛出异常，失败原因在message中
     */
    int remove(Integer id) throws Exception;

    /**
     *
     * @param staff
     * @return
     * @throws Exception
     */
    int edit(Staff staff) throws Exception;

    /**
     *
     * @param staff
     * @return
     * @throws Exception
     */
    Staff login(Staff staff) throws Exception;
    /**
     *
     * @param id
     * @return
     */
    Staff getById(Integer id);

    /**
     *
     * @return
     */
    List getAll(Staff staff);


    /**
     * 修改密码
     * @param oldPwd
     * @param newPwd
     * @param id
     * @return
     * @throws Exception
     */
    int resetPwd(String oldPwd, String newPwd,Integer id) throws Exception;

    /**
     * 分页查询
     *
     * @param page
     * @param limit
     * @param staff
     * @return
     */

    PageInfo getForPage(Integer page, Integer limit, Staff staff);


    List getByDeptId(Integer sId);
}
