package com.situ.ward.filter;

import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;

@WebFilter(filterName = "LogFilter",urlPatterns = "/*")
public class LogFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        //打印日志
        HttpServletRequest req = (HttpServletRequest) request;
        //访问路径
        System.out.println(req.getRequestURI());
        //传递参数
        for (String name:req.getParameterMap().keySet()) {
            System.out.println(name+"-"+ Arrays.toString(req.getParameterMap().get(name)));
        }
        System.out.println(req.getParameterMap());
        //请求头
        System.out.println(req.getHeader("user-agent"));
        //客户端的IP地址
        System.out.println(req.getRemoteHost());
        chain.doFilter(request, response);
    }
}
