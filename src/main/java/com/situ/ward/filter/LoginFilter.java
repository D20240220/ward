package com.situ.ward.filter;

import com.situ.ward.entity.Staff;
import com.situ.ward.service.StaffService;
import com.situ.ward.service.impl.StaffServiceImpl;
import com.situ.ward.util.CookieUtils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 *
 * 登录时放行
 *
 */

public class LoginFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        //如果登录页面则直接放行
        if (request.getRequestURI().endsWith("login.jsp")) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;//后面的代码就不需要执行了
        }
        String requestURI = request.getRequestURI();
        System.out.println("requestURI:" + requestURI);
        String method = request.getParameter("action");
        //这是请求是要去完成登录的，不需要执行后面验证是否登录的流程
        if (requestURI.startsWith("/static")
                || requestURI.equals("/verCode")) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
        //判断用户是否登录
        HttpSession session = request.getSession();
        Staff staff = (Staff) session.getAttribute("staff");

        if (staff == null) {
            Cookie cookie = CookieUtils.getCookie(request, "autoLogin");
            if (cookie != null) {
                //存在自动登录
                String[] values = cookie.getValue().split("&");
                String name = values[0];
                String password = values[1];
                staff.setName(name);
                staff.setPassword(password);
                StaffService staffService = new StaffServiceImpl();
                try {
                    staff = staffService.login(staff);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                if (staff != null) {
                    session.setAttribute("staff", staff);
                    filterChain.doFilter(servletRequest, servletResponse);
                    return;
                }
            }
            response.sendRedirect("staff/login.jsp");
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }


    @Override
    public void destroy() {
        Filter.super.destroy();
    }
}
