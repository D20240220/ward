package com.situ.ward.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
public class Bed {
    private Integer id;
    private String name;
    private String bed;
    private Integer dept_id;
    private Integer status;
}
