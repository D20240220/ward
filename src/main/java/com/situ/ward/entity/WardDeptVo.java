package com.situ.ward.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WardDeptVo {
    private Integer id;
    private String name;
    private Integer dept_id;
    private Integer status;
    private String dept_name;

}
