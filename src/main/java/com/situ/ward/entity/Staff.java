package com.situ.ward.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 实体类
 */
@Data
@NoArgsConstructor
@ToString
public class Staff {
    private Integer id;
    private String password;
    private String name;
    private String sex;
    private String phone;
    private String email;
    private String head;
    private Integer role;
    private Integer status;
    private Integer dept_id;
}
