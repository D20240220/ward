package com.situ.ward.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result {
    private Integer code;
    private String msg;
    private Object data;

    // 快速使用的方法
    public static Result ok(){
        return ok(null);
    }
    public static Result ok(Object data){
        return ok(0, null, data);
    }
    public static Result ok(Integer code, String msg, Object data){
        return new Result(code, msg, data);
    }

    public static Result error(String msg){
        return error(1, msg);
    }
    public static Result error(Integer code, String msg){
        return error(code, msg, null);
    }
    public static Result error(Integer code, String msg, Object data){
        return new Result(code, msg, data);
    }

    // 将当前对象转换成JSON格式的字符串
    public String toJson() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(this);
    }
}
