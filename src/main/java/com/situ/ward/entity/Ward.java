package com.situ.ward.entity;




public class Ward {
    private Integer id;
    private String name;
    private Integer dept_id;
    private Integer status;

    public Ward() {
    }

    public Ward(Integer id, String name, Integer dept_id, Integer status) {
        this.id = id;
        this.name = name;
        this.dept_id = dept_id;
        this.status = status;
    }

    /**
     * 获取
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return dept_id
     */
    public Integer getDept_id() {
        return dept_id;
    }

    /**
     * 设置
     * @param dept_id
     */
    public void setDept_id(Integer dept_id) {
        this.dept_id = dept_id;
    }

    /**
     * 获取
     * @return status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置
     * @param status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    public String toString() {
        return "Ward{id = " + id + ", name = " + name + ", dept_id = " + dept_id + ", status = " + status + "}";
    }
}
