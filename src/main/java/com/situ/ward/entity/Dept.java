package com.situ.ward.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


public class Dept {
    private Integer id;
    private String name ;
    private String room;
    private String phone;
    private String email;
    private Integer status;

    public Dept() {
    }

    public Dept(Integer id, String name, String room, String phone, String email, Integer status) {
        this.id = id;
        this.name = name;
        this.room = room;
        this.phone = phone;
        this.email = email;
        this.status = status;
    }

    /**
     * 获取
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return room
     */
    public String getRoom() {
        return room;
    }

    /**
     * 设置
     * @param room
     */
    public void setRoom(String room) {
        this.room = room;
    }

    /**
     * 获取
     * @return phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 设置
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 获取
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * 设置
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 获取
     * @return status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置
     * @param status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    public String toString() {
        return "Dept{id = " + id + ", name = " + name + ", room = " + room + ", phone = " + phone + ", email = " + email + ", status = " + status + "}";
    }
}
