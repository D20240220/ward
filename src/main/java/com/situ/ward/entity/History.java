package com.situ.ward.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@ToString
public class History {
    private Integer id;
    private String id_card;
    private String staffname;
    private String ward;
    private String name;
    private String bed;
    private String deptname;
    private Integer bed_id;
    private Integer staff_id;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime intime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime outtime;
}
